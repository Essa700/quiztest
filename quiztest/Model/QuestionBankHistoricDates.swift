import Foundation



class QuestionBankHistoricDates {
    
    var questions = [Question]()
    
    init() {
        
        questions.append(Question(questionText: "The first FIFA World Cup final took place on July 30, 1930, in Uruguay.", answer: true))
        
        questions.append(Question(questionText: "The Ford Model T, the car that ushered in the automobile era, was launched on September 27, 1938.", answer: false))
        
        questions.append(Question(questionText: "On October 29, 1929, the Wall Street stock market crash marked the beginning of the Great Depression.", answer: true))
        
        questions.append(Question(questionText: "On Friday, November 22, 1963, John F. Kennedy was assassinated in Dallas.", answer: true))
        
        questions.append(Question(questionText: "The U.S. dropped an atomic bomb on Hiroshima on August 6, 1945.", answer: true))
        
        questions.append(Question(questionText: "The Jazz Singer,the first talking film, was released on October 6, 1947.", answer: false))
            
            questions.append(Question(questionText: "On August 20, 1968, Soviet troops invaded Hungary.", answer: false))
            
            questions.append(Question(questionText: "The Challenger space shuttle disaster occurred on January 28, 1986.", answer: true))
            
            questions.append(Question(questionText: "The Disney movie Snow White and the Seven Dwarfs was released on December 21, 1937.", answer: true))
            
            questions.append(Question(questionText: "The end of the First World War and the attack on the World Trade Center both took place on September 11.", answer: false))
            
            questions.append(Question(questionText: "On April 12, 1961, Russian cosmonaut Yuri Gagarin became the first human in space.", answer: true))
            
            questions.append(Question(questionText: "On July 5, 1996, Dolly the sheep, the first mammal to be cloned from an adult cell, was born.", answer: true))
            
            questions.append(Question(questionText: "The Titanic sank on Christmas Day in 1925.", answer: false))
            
            questions.append(Question(questionText: "On January 1, 1900, Orville Wright made the first controlled flight of a power-driven airplane.", answer: false))
            
            questions.append(Question(questionText: "On April 30, 1975, the city of Saigon (Ho Chi Minh City) surrendered to communist forces.", answer: true))
            
            questions.append(Question(questionText: "On August 9, 1980, U.S. president Richard Nixon resigned.", answer: false))
            
            questions.append(Question(questionText: "On February 11, 1990, Nelson Mandela was released after 27 years in prison.", answer: true))
            
            questions.append(Question(questionText: "On May 29, 1953, Edmund Hillary and Tenzing Norgay reached the summit of Mount Everest.", answer: true))
            
            questions.append(Question(questionText: "India became independent on the night of August 14–15, 1947.", answer: true))
            
            questions.append(Question(questionText: "On January 1, 1959, Cuban dictator Fulgencio Batista fled to the Dominican Republic.", answer: true))
        
    }
    
}
