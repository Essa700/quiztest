import Foundation



class QuestionBankFood {
    
    var questions = [Question]()
    
    init() {
        
        questions.append(Question(questionText: "On average, there are 200 tiny seeds on every strawberry. ", answer: true))
        
        questions.append(Question(questionText: "Christopher Columbus brought pasta back to Italy from Brazil.", answer: false))
        
        questions.append(Question(questionText: "Ketchup was once advertised as medicine.", answer: true))
        
        questions.append(Question(questionText: "The Aztecs were the first to serve chocolate as a drink.", answer: true))
        
        questions.append(Question(questionText: "There are about 700 varieties of apples in the world.", answer: false))
        
        questions.append(Question(questionText: "The melting point of chocolate is just below our natural body temperature.", answer: true))
        
        questions.append(Question(questionText: "An ear of corn always has an uneven number of rows.", answer: false))
        
        questions.append(Question(questionText: "The tea bag was introduced by mistake. ", answer: true))
        
        questions.append(Question(questionText: "Tomatoes were introduced in Italy by Marco Polo after he took a trip to China. ", answer: false))
        
        questions.append(Question(questionText: "A harvested apple tree produces about 220 lb. (100 kg) of apples yearly.", answer: false))
        
        questions.append(Question(questionText: "Peanuts can be used to make dynamite.", answer: true))
        
        questions.append(Question(questionText: "Coffee is a zero-calorie beverage. ", answer: true))
        
        questions.append(Question(questionText: "In New Zealand, there is one sheep for every person in the country.", answer: false))
        
        questions.append(Question(questionText:  "Fresh apples float. ", answer: true))
        
        questions.append(Question(questionText: "The red juice that comes out of a steak is not blood.", answer: true))
        
        questions.append(Question(questionText: "Neapolitan sauce is named after Napoleon, who was a great fan of the sauce. ", answer: false))
        
        questions.append(Question(questionText: "Mozzarella is made with goat milk. ", answer: false))
        
        questions.append(Question(questionText: "To make 2.2 lb. (1 kg) of honey, bees have to visit 4 million flowers. ", answer: true))
        
        questions.append(Question(questionText: "The Earl of Sandwich invented sandwiches.", answer: true))
        
        questions.append(Question(questionText: "A watermelon is about 55% water by weight. ", answer: false))
        
    }
    
}

