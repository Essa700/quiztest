import Foundation



class QuestionBankAnimals {
    
    var questions = [Question]()
    
    init() {
        
        questions.append(Question(questionText: "Like humans, horses do not see well at night.", answer: false))
        
        questions.append(Question(questionText: "A bull can’t recognize the colour red.", answer: true))
        
        questions.append(Question(questionText: "The hammerhead shark has poor eyesight.", answer: false))
        
        questions.append(Question(questionText: "Dogs only see in black and white.", answer: false))
        
        questions.append(Question(questionText: "A hawk can spot rodents from a distance of 3,000 m.", answer: true))
        
        questions.append(Question(questionText: "During the day, cats have poor vision both near and far.", answer: true))
        
        questions.append(Question(questionText: "An owl is unable to move its eye, it has to move its head to change its angle of view.", answer: true))
        
        questions.append(Question(questionText: "The bowhead whale relies on its sharp sense of sight to swim in the dark waters of the Arctic.", answer: false))
        
        questions.append(Question(questionText: "A bee is attracted to the colour red in flowers.", answer: false))
        
        questions.append(Question(questionText: "The eyes of the seahorse operate independently from one another. ", answer: true))
        
        questions.append(Question(questionText: "A bear mainly uses its eyes to hunt, it stands up to see farther.", answer: false))
        
        questions.append(Question(questionText: "A tiger’s eyes are similar to those of a cat.", answer: false))
        
        questions.append(Question(questionText: "The night vision of a cat is 6 to 7 times better than that of a human.", answer: true))
        
        questions.append(Question(questionText: "An octopus perceives the variations of light with its skin.", answer: true))
        
        questions.append(Question(questionText: "Despite its huge eyes, the dragonfly has poor sight.", answer: false))
        
        questions.append(Question(questionText: "The carpet python can hunt in complete darkness.", answer: true))
        
        questions.append(Question(questionText: "The blue whale, the largest animal in the world, has minuscule eyes. ", answer: false))
        
        questions.append(Question(questionText: "A tiger’s eyes are similar to those of a cat.", answer: false))
        
        questions.append(Question(questionText: "A chameleon can see in front and back at the same time without moving its head.", answer: true))
        
        questions.append(Question(questionText: "The cheetah has the best night vision among all felines.", answer: false))
        
    }
    
}

