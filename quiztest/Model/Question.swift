//
//  Question.swift
//  quiztest
//
//  Created by Mirza.com on 2018-11-12.
//  Copyright © 2018 Mirza.com. All rights reserved.
//

import Foundation

class Question {
    var questionText: String
    var answer : Bool
    
    init(questionText: String, answer: Bool) {
        self.questionText = questionText
        self.answer = answer
    }
}
