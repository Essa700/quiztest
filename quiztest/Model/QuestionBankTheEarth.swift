import Foundation



class QuestionBankTheEarth {
    
    var questions = [Question]()
    
    init() {
        
        questions.append(Question(questionText: "Angel Falls are so high that, on warm days, water evaporates before reaching the stream below.", answer: true))
        
        questions.append(Question(questionText: "The Great Barrier Reef is visible from space.", answer: true))
            
            questions.append(Question(questionText: "The Dead Sea is filled with exotic fish.", answer: false))
            
            questions.append(Question(questionText: "Each year, there are about 500,000 earthquakes around the world. ", answer: true))
            
            questions.append(Question(questionText: "The Arctic is colder than Antarctica.", answer: false))
            
            questions.append(Question(questionText: "The ocean surface temperature varies between 28,4 °F (−2 °C) and 95 °F (35 °C).", answer: true))
            
            questions.append(Question(questionText: "On average, there are 5 lightning strikes every second on Earth. ", answer: true))
            
            questions.append(Question(questionText: "The Moon revolves around Earth in a little more than 27 days. ", answer: true))
            
            questions.append(Question(questionText: "Earth is the fourth planet closest to the Sun. ", answer: false))
            
            questions.append(Question(questionText: "The colors of a rainbow always appear in the same order. ", answer: true))
            
            questions.append(Question(questionText: "Forests cover about 70% of Earth’s land. ", answer: false))
            
            questions.append(Question(questionText: "The Sahara desert is smaller than Mexico.", answer: false))
            
            questions.append(Question(questionText: "Africa covers 14% of Earth’s surface.", answer: false))
            
            questions.append(Question(questionText: "Earth’s orbital speed is 18.54 mi. (29.85 km) per second.", answer: true))
            
            questions.append(Question(questionText: "What we call “salt deserts” are in fact white rock deserts. ", answer: false))
            
            questions.append(Question(questionText: "The Mauna Kea volcano in Hawaii can be considered taller than Mount Everest. ", answer: true))
            
            questions.append(Question(questionText: "The distance between the Sun and Earth is 54.7 million mi. (88 million km). ", answer: false))
            
            questions.append(Question(questionText: "The Atacama Desert in Chile is the driest place on Earth.", answer: true))
            
            questions.append(Question(questionText: "The Niagara Falls flow is about 26,400 US gallons (100,000 liters) per second.", answer: false))
            
            questions.append(Question(questionText: "It takes about 8 seconds for the Sun’s rays to reach Earth. ", answer: false))
        
    }
    
}

