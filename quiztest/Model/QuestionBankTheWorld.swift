import Foundation



class QuestionBankTheWorld {
    
    var questions = [Question]()
    
    init() {
        
        questions.append(Question(questionText: "The world’s tallest waterfalls are located in Venezuela.", answer: true))
        
        questions.append(Question(questionText: "Since February 2015, India is the most densely populated country in the world.", answer: false))
        
        questions.append(Question(questionText: "About 100 languages are spoken in New Guinea.", answer: false))
        
        questions.append(Question(questionText: "Tokyo is the most densely populated city in the world.", answer: true))
        
        questions.append(Question(questionText: "Ethiopia is considered to be the cradle of humanity.", answer: true))
        
        questions.append(Question(questionText: "The southern end of Argentina touches both the Pacific Ocean and the Atlantic Ocean.", answer: false))
        
        questions.append(Question(questionText: "Climate change threatens the Republic of Maldives.", answer: true))
        
        questions.append(Question(questionText: "Easter Island is located 435 mi. (700 km) off Brazil, in the Atlantic Ocean.", answer: false))
        
        questions.append(Question(questionText: "The Mayan civilization was born in Laos.", answer: false))
        
        questions.append(Question(questionText: "Italy has the oldest active volcano in Europe.", answer: true))
        
        questions.append(Question(questionText: "Mongolia is the least densely populated country.", answer: true))
        
        questions.append(Question(questionText: "China is the largest country in the world.", answer: false))
        
        questions.append(Question(questionText: "The world’s population is about 7.3 billion people.", answer: true))
        
        questions.append(Question(questionText: "About 50% of all countries are located north of the equator.", answer: false))
        
        questions.append(Question(questionText: "In 1970, the world's population was half of what it is today.", answer: true))
        
        questions.append(Question(questionText: "The Taj Mahal is located near Mumbai (Bombay), on the Red Sea coast.", answer: false))
        
        questions.append(Question(questionText: "China has 14 neighboring countries.", answer: true))
        
        questions.append(Question(questionText: "Portuguese is the official language of ten countries.", answer: true))
        
        questions.append(Question(questionText: "The world’s population is about 7.3 billion people.", answer: false))
        
        questions.append(Question(questionText: "More than half of the world’s population lives in urban areas.", answer: true))
        
    }
    
}

