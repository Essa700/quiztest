//
//  ViewController.swift
//  quiztest
//
//  Created by Mirza.com on 2018-11-11.
//  Copyright © 2018 Mirza.com. All rights reserved.
//

import UIKit
import FBAudienceNetwork
import PKHUD


class ViewController: UIViewController,FBAdViewDelegate {
    
    var questions = [Question]()
    var category = ""
    
    var bannerAdView:FBAdView!
    var allQuestion = QuestionBankFood()
    var answerPressed: Bool = false
    var questionNumber:Int = 0
    var score:Int = 0
    
    @IBOutlet weak var trueButton: UIButton!
    @IBOutlet weak var falseButton: UIButton!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var progressBar: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressView: UIView!
    
    @IBOutlet weak var coverView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        updateUi()
        
        if(category == "food")
        {
            questions = QuestionBankFood().questions
        }
        
        if(category == "animals")
        {
            questions = QuestionBankAnimals().questions
        }
        
        if(category == "world")
        {
            questions = QuestionBankTheWorld().questions
        }
        
        if(category == "history")
        {
            questions = QuestionBankHistoricDates().questions
        }
        
        if(category == "earth")
        {
            questions = QuestionBankTheEarth().questions
        }
        
        
        questionLabel.layer.cornerRadius = 20.0
        questionLabel.layer.masksToBounds = true
        
        progressBar.layer.cornerRadius = 20.0
        progressBar.layer.masksToBounds = true
        
        self.applyRoundButton(trueButton)
        self.applyRoundButton(falseButton)
        
        
        loadFacebookbanner()
        
        
        UIView.animate(withDuration: 0.2) {
            self.coverView.alpha = 0
        }
        questionLabel.text = allQuestion.questions[questionNumber].questionText
    }
    func loadFacebookbanner() {
        bannerAdView = FBAdView(placementID: "1173761142778076_1173763936111130", adSize: kFBAdSizeHeight50Banner, rootViewController: self)
        
        bannerAdView.frame = CGRect(x:0, y:view.bounds.height - bannerAdView.frame.size.height, width: bannerAdView.frame.width, height: bannerAdView.frame.size.height)
        bannerAdView.delegate = self
        bannerAdView.isHidden = true
        self.view.addSubview(bannerAdView)
        bannerAdView.loadAd()
    }
    func adViewDidLoad(_ adView: FBAdView) {
        bannerAdView.isHidden = false
    }
    func adView(_ adView: FBAdView, didFailWithError error: Error) {
        print(error)
    }
    @IBAction func answerPressed(_ sender: AnyObject) {
        if sender.tag == 1 {
            answerPressed = true
            
        } else if sender.tag == 2 {
            answerPressed = false
            
        }
        checkAnswer()
        nextQuestion()
       updateUi()
        
        
    }
    
    func updateUi() {
        progressLabel.text = "Q:  \(questionNumber + 1)/20"
        scoreLabel.text = "Score: \(score)"
        
        
        
    }
    func applyRoundButton(_ object:AnyObject) {
        
        trueButton.layer.cornerRadius = falseButton.frame.size.width / 2
        trueButton.layer.masksToBounds = true
        
        falseButton.layer.cornerRadius = falseButton.frame.size.width / 2
        falseButton.layer.masksToBounds = true
    }
    
    func nextQuestion(){
        if questionNumber < 19 {
            questionNumber += 1
            questionLabel.text = allQuestion.questions[questionNumber].questionText
        } else {
            print("Quiz Is Over")
            
            UIView.animate(withDuration: 0.2) {
                self.coverView.alpha = 1
            }
            questionLabel.text = " "
            
            let alert = UIAlertController(title: "🌹 THANKS 🌹", message: "Quiz Is Over", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Go Back And Choose Another Category", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
                
                
                UIView.animate(withDuration: 0.2) {
                    self.coverView.alpha = 0
                }
                self.questionNumber = 0
                self.score = 0
                self.questionLabel.text = self.allQuestion.questions[self.questionNumber].questionText
                
                self.updateUi()
            }))
            self.present(alert, animated: true, completion: nil)
            
            
            
        }
        
    }
    
    
    
    
    func checkAnswer() {
        if answerPressed == allQuestion.questions[questionNumber].answer {
            PKHUD.sharedHUD.contentView = PKHUDSuccessView()
            PKHUD.sharedHUD.show()
            PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
            }
            print("jag har rätt")
            score += 1
        } else {
            PKHUD.sharedHUD.contentView = PKHUDErrorView()
            PKHUD.sharedHUD.show()
            PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                print("jag har fel")
            }
        }
        
    }
    func startOver(){
        UIView.animate(withDuration: 0.2) {
            self.coverView.alpha = 0
        }
        questionNumber = 0
        score = 0
        questionLabel.text = allQuestion.questions[questionNumber].questionText
        updateUi()
    }
}
