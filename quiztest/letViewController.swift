//
//  letViewController.swift
//  quiztest
//
//  Created by Mirza.com on 2018-11-16.
//  Copyright © 2018 Mirza.com. All rights reserved.
//

import UIKit

class letViewController: UIViewController {

    //@IBOutlet weak var letPlay: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       //letPlay.layer.cornerRadius = 30.0
       //letPlay.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func goAnimals(_ sender: Any) {
        performSegue(withIdentifier: "questions", sender: "animals")
    }
    
    @IBAction func goFood(_ sender: Any) {
        performSegue(withIdentifier: "questions", sender: "food")
    }
    
    @IBAction func goEarth(_ sender: Any) {
        performSegue(withIdentifier: "questions", sender: "earth")
    }
    
    @IBAction func goHistory(_ sender: Any) {
        performSegue(withIdentifier: "questions", sender: "history")
    }
    @IBAction func goWorldWide(_ sender: Any) {
        performSegue(withIdentifier: "questions", sender: "world")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dest = segue.destination as! ViewController
        dest.category = sender as! String
    }
    

}
